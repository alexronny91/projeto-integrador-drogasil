#language: pt

@login
Funcionalidade: Fazer Login 
  EU, como cliente.
  QUERO ter uma tela onde possa realizar o acesso com meu login e senha previamente cadastrado
  PARA que eu possa realizar compras no sistema

  @logar
  Cenario: Realizando Login
  Dado que estou na tela de login da Drogasil
  Quando insiro meus dados de acesso e faço login
  Então verifico que estou logado
