#language: pt

@logout
Funcionalidade: Fazer Logout 
  EU, como cliente.
  QUERO ter um link ou botão onde eu possa fazer logout no sistema
  PARA que ninguém utilize minha conta para realizar ou visualizar minha compras

  @deslogar
  Cenario: Realizando Logout
  Dado que estou logado no site da Drogasil
  Quando passo o mouse sobre o campo Ola, Meu Nome e clico em Sair
  Então verifico que estou desconectado